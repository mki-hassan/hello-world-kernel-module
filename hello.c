/*  
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */

// The first function that runs when we insmode a kernel module
int init_module(void)
{
	printk(KERN_INFO "Hello world from Hassan.\n");

	return 0;
}

// The function that runs when rmmod the kernel module
void cleanup_module(void)
{
	printk(KERN_INFO "Thanks for using this module. \n");
}
MODULE_LICENSE("GPL");